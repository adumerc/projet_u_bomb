package fr.ubx.poo.ubomb.go.decor;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Level;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.character.Player;

public class Box extends Decor {
    private Direction direction = null;
    private Game game;
    private boolean moveRequested=false;

    public void setGame(Game game) {
        this.game = game;
    }

    public Box(Position position) {
        super(position);
    }

    public boolean walkableBy(Player player) {
        return true;
    }

    @Override
    public boolean isRemovable() {
        return false;
    }

    @Override
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isBox() {
        return true;
    }

    public void doMoveBox(Direction direction) {
        Position nextPos = direction.nextPosition(getPosition());
        setPosition(nextPos);
        game.grid().set(nextPos,this);
    }

    public final boolean canMoveBox(Direction direction) {
        Position nextPos = direction.nextPosition(getPosition());
        if (game != null) {
            GameObject next = game.grid().get(nextPos);
            if (!game.grid().inside(nextPos)) {
                return false;
            }
            if (next == null) {
                return true;
            }
            return next.walkableBy(this);

        }
        return false;
    }

    public void requestMoveBox(Direction direction) {
        if (direction != this.direction) {
            this.direction = direction;
            setModified(true);
            this.moveRequested=true;
            updateBox();
            this.direction=null;
        }



        this.moveRequested=false;
    }

    public void updateBox() {
        if (moveRequested) {
            if (canMoveBox(direction)) {
                doMoveBox(direction);
                System.out.println("x: "+getPosition().x()+" y: "+getPosition().y());

            }
        }
    }
}




