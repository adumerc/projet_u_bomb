package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.Takeable;
import fr.ubx.poo.ubomb.go.Walkable;
import fr.ubx.poo.ubomb.go.character.Player;
import fr.ubx.poo.ubomb.go.decor.Decor;
import fr.ubx.poo.ubomb.go.decor.bonus.Bonus;

public class Monster extends Bonus implements Walkable, Takeable {
    public Monster(Position position) {
        super(position);
    }

    Direction direction;

    public Direction getDirection() {
        return null;
    }

    @Override
    public void takenBy(Player player) {
        player.take(this);
    }

    @Override
    public boolean isRemovable(){
        return false;
    }

}
