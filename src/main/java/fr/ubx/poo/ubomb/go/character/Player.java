/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.Movable;
import fr.ubx.poo.ubomb.go.TakeVisitor;
import fr.ubx.poo.ubomb.go.decor.Box;
import fr.ubx.poo.ubomb.go.decor.bonus.*;


public class Player extends GameObject implements Movable, TakeVisitor {

    private Direction direction;
    private boolean moveRequested = false;
    private int lives;
    private int bombRange;
    private int availableBombs;

    public int getAvailableBombs() {
        return availableBombs;
    }

    public void setAvailableBombs(int availableBombs) {
        this.availableBombs = this.availableBombs + availableBombs;
    }

    public void increaseAvailableBombs(){
        setAvailableBombs(1);
    }
    public void decreaseAvailableBombs(){
        setAvailableBombs(-1);
    }

    public int getBombRange() {
        return bombRange;
    }

    public void setBombRange(int bombRange) {
        this.bombRange = bombRange;
    }

    public void increaseBombRange(){
        setBombRange(1 + this.getBombRange());
    }
    public void decreaseBombRange(){
        setBombRange( this.getBombRange()-1);
    }




    public int getKeys() {
        return keys;
    }

    private int keys;
    private boolean isWin = false;

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public Player(Game game, Position position) {
        super(game, position);
        this.direction = Direction.DOWN;
        this.lives = game.configuration().playerLives();
    }


    @Override
    public void take(Key key) {
        System.out.println("Take the key ...");
        this.increaseKeys();
    }

    private void increaseKeys() {
        this.keys++;
    }

    public void take(Heart heart) {
        System.out.println("Heart ...");
        this.increaseLives();
    }
    public void take(Monster monster) {
        System.out.println("Monster ...");
        this.decreaseLives();
    }
    public void take(Princess princess) {
        System.out.println("Princess ...");
        setWin(true);
    }

    public void take(BombNumberInc bombNumberInc) {
        System.out.println("bombNumberInc ...");
        this.increaseAvailableBombs();
    }
    public void take(BombNumberDec bombNumberDec) {
        System.out.println("bombNumberDec ...");
        this.decreaseAvailableBombs();
    }
    public void take(BombRangeDec bombRangeDec) {
        System.out.println("bombRangeDec ...");
        this.decreaseBombRange();
    }
    public void take(BombRangeInc bombRangeInc) {
        System.out.println("bombRangeInc ...");
        this.increaseBombRange();
    }
    public void take(DoorNextClosed doorNextClosed) {
        System.out.println("doorNextClosed ...");
    }
    public void take(DoorNextOpened doorNextOpened) {
        System.out.println("doorNextOpened ...");
    }


    public void doMove(Direction direction) {
        // This method is called only if the move is possible, do not check again
        Position nextPos = direction.nextPosition(getPosition());
        GameObject next = game.grid().get(nextPos);
        if (next instanceof Bonus bonus) {
            bonus.takenBy(this);
        }
         if (next != null && next.isBox()){
            next.setGame(this.game);
            next.requestMoveBox(this.direction);
             System.out.println("x:"+getPosition().x()+"y:"+getPosition().y());
             System.out.println("is box"+next.isBox());


        }
        setPosition(nextPos);
    }


    public int getLives() {
        return lives;
    }

    public Direction getDirection() {
        return direction;
    }

    public void requestMove(Direction direction) {
        if (direction != this.direction) {
            this.direction = direction;
            setModified(true);
        }
        moveRequested = true;
    }

    public final boolean canMove(Direction direction) {

        Position nextPos = direction.nextPosition(getPosition());

        GameObject next = game.grid().get(nextPos);
        if (!game.grid().inside(nextPos)){
            return false;
        }
        if(next==null){
            return true;
        }

        return next.walkableBy(this);

    }

    public void update(long now) {
        if (moveRequested) {
            if (canMove(direction)) {
                doMove(direction);
            }
        }
        moveRequested = false;

        Position Pos = getPosition();

        GameObject Object = game.grid().get(Pos);

        if(Object != null && Object.walkableBy(this) && Object.isRemovable()){
            Object.remove();
        }


    }

    @Override
    public void explode() {
        // TODO
    }

    public void setLives(int i) {
        lives = lives + i;
    }
    public void setKeys(int i) {
        this.keys = this.keys + i;
    }


    public void increaseLives(){
        setLives(1);
    }
    public void decreaseLives(){
        setLives(-1);
    }
}
