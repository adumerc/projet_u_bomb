package fr.ubx.poo.ubomb.launcher;

import fr.ubx.poo.ubomb.game.Grid;

public class MapLevel {

    private final int width;
    private final int height;
    private final int level;
    private final Entity[][][] grid;

    public MapLevel(int level, int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new Entity[level][height][width];
        this.level = level;
    }

    public int width() {
        return width;    }

    public int height() {
        return height;
    }

    public int level() {
        return level;
    }

    public Entity get(int level, int i, int j) {
        return grid[level][j][i];
    }

    public void set(int level,int i, int j, Entity entity) {
        grid[level][j][i] = entity;
    }

}
