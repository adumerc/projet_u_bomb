package fr.ubx.poo.ubomb.launcher;

import fr.ubx.poo.ubomb.game.Configuration;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Level;
import fr.ubx.poo.ubomb.game.Position;

import java.io.*;
import java.util.Properties;

public class GameLauncher {

    public static Game load() {
        Configuration configuration = new Configuration(new Position(0, 0), 3, 3, 4000, 5, 1000);
        return new Game(configuration, new Level(new MapLevelDefault()));
    }

    public static Game load(File file) {
        Properties config = new Properties();
        Reader in = null;
        try {
            in = new FileReader(file);
            config.load(in);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        boolean compression = Boolean.parseBoolean(config.getProperty("compression", "false"));
        String position= config.getProperty("player");
        String p1 = "";
        String p2 = "";
        int i = 0;
        while(position.charAt(i)!= 'x'){
            p1 = p1 + position.charAt(i);
            i++;
        }
        i++;
        while(i<position.length()){
            p2 = p2 + position.charAt(i);
            i++;
        }
        int pos1 = Integer.parseInt(p1);
        int pos2 = Integer.parseInt(p2);

        System.out.println(p1);
        System.out.println(p2);

        int levels = Integer.parseInt(config.getProperty("levels", "1"));
        int bombBagCapacity = Integer.parseInt(config.getProperty("bombBagCapacity", "3"));
        int playerLives = Integer.parseInt(config.getProperty("playerLives", "5"));
        long playerInvisibilityTime = Long.parseLong(config.getProperty("playerInvisibilityTime", "4000"));
        int monsterVelocity = Integer.parseInt(config.getProperty("monsterVelocity", "5"));
        long monsterInvisibilityTime = Long.parseLong(config.getProperty("monsterInvisibilityTime", "1000"));
        Configuration configuration = new Configuration(new Position(pos1, pos2), bombBagCapacity, playerLives, playerInvisibilityTime, monsterVelocity, monsterInvisibilityTime);
        return new Game(configuration, new Level(new MapLevelDefault()));
    }


}
